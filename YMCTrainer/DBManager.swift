import Foundation

public final class DBManager : NSObject {
   
   public static func saveDic(dic: AnyObject, userDefaultsKey : String) {
      print("SAVING " + userDefaultsKey)
      do {
         print(history.description)
         let data = try PropertyListSerialization.data(fromPropertyList: dic, format:PropertyListSerialization.PropertyListFormat.binary, options: 0)
         UserDefaults.standard.set(data, forKey:userDefaultsKey)
         UserDefaults.standard.synchronize()
      } catch {
         print("Error reading plist: \(error)")
      }
   }
   
   public static func loadDic(userDefaultsKey : String) -> NSMutableDictionary {
      print("LOAD " + userDefaultsKey)
      var out : NSMutableDictionary?
      var format = PropertyListSerialization.PropertyListFormat.binary
      if let plistXML = UserDefaults.standard.object(forKey: userDefaultsKey) as? NSData {
         do {
            out = try PropertyListSerialization.propertyList(from: plistXML as Data, options: .mutableContainersAndLeaves, format: &format) as? NSMutableDictionary
         }
         catch {
            print("Error reading plist: \(error), format: \(format)")
         }
      }
      if out != nil {
         return out!
      } else {
         return NSMutableDictionary()
      }
   }
}
