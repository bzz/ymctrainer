import UIKit


public class NavigationVC : UIViewController
{
   
   
   
   init(initialButtonIndex : Int) {
      super.init(nibName: nil, bundle: nil)
      currentButtonTag = initialButtonIndex
      self.startVC = vcForButtonIndex(buttonIndex: initialButtonIndex)
   }
   
   
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   
   
   let btnHeight :CGFloat = 49 * UIScreen.main.bounds.width / 320
   let btnWidth  :CGFloat = 64 * UIScreen.main.bounds.width / 320
   
   var topPanel:UIView!
   var addButton:UIButton!
   var titleButton:UIButton!
   var newMessageIndicator:UIView!
   var onlineIndicator:UIImageView!
   
   
   
   
   
   var bottomPanel:UIView!
   var currentButtonTag = 0
   
   var mainViewBed = UIView()
   var currentVC:UIViewController!
   
   
   var startVC : UIViewController?
   
   
   override public func viewDidLoad() {
      
      
      let background = UIImageView(frame:view.bounds)
      background.image = UIImage(named:"background");
      view.addSubview(background)
      
      
      
      mainViewBed = UIView(frame: CGRect(x:0, y:0, width:screenWidth, height:screenHeight-btnHeight))
      mainViewBed.backgroundColor = UIColor.clear
      view.addSubview(mainViewBed)
      
      
      
      bottomPanel = UIView(frame: CGRect(x: 0, y: screenHeight-btnHeight, width: screenWidth, height: btnHeight))
      bottomPanel.backgroundColor = UIColor(hex: 0xffffff)
      view.addSubview(bottomPanel);
      
      
      //TODO: FIX SELECTED IMAGES
      addPanelButton(tag: 0, image:"bbttn0", imageSelected: "bbttn0")
      addPanelButton(tag: 4, image:"bbttn4", imageSelected: "bbttn4")
      
      if (startVC != nil) {
         let toVC = startVC!
         self.addChildViewController(toVC)
         self.mainViewBed.addSubview(toVC.view)
         toVC.didMove(toParentViewController: self)
         self.currentVC = toVC
      }
      
      
   }
   
   
   func vcForButtonIndex(buttonIndex : Int) -> UIViewController {
      var toVC = UIViewController()
      switch buttonIndex {
      case 0:
         toVC = AddVC()
      case 4:
         toVC = StatsVC()
         
      default: break }
      
      return toVC
   }
   
   
   
   func addPanelButton(tag:NSInteger, image:String, imageSelected:String) {
      let button = UIButton(frame: CGRect(x: btnWidth*CGFloat(tag), y: 0, width: btnWidth, height: btnHeight))
      button.setBackgroundImage(UIImage(named: image), for: .normal)
      button.setBackgroundImage(UIImage(named: imageSelected), for: .selected)
      button.adjustsImageWhenHighlighted = false
      button.addTarget(self, action:#selector(NavigationVC.panelButtonPressed), for: UIControlEvents.touchDown)
      button.tag = tag
      if tag == currentButtonTag {
         button.isSelected = true
      }
      bottomPanel.addSubview(button)
   }
   
   func panelButtonPressed(button:UIButton) {
      for v:UIView in bottomPanel.subviews {
         if v.isKind(of: UIButton.self) {
            let b = v as! UIButton
            b.isSelected = false
         }
      }
      button.isSelected = true;
      
      
      let toVC = vcForButtonIndex(buttonIndex: button.tag)
      if currentVC.classForCoder == toVC.classForCoder {return}
      
      
      self.currentVC!.willMove(toParentViewController: nil)
      self.currentVC!.removeFromParentViewController()
      self.addChildViewController(toVC)
      self.mainViewBed.addSubview(toVC.view)
      
      
      let bedCenter = mainViewBed.convert(mainViewBed.center, from: view)
      
      let completion = { (value: Bool) in
         self.currentVC!.view.removeFromSuperview()
         self.currentVC = toVC
         self.bottomPanel.isUserInteractionEnabled = true
      }
      
      self.bottomPanel.isUserInteractionEnabled = false
      toVC.view.center.y = bedCenter.y
      
      if self.currentButtonTag < button.tag {
         toVC.view.center.x = screenWidth + bedCenter.x
         UIView.animate(withDuration: 0.25, animations: {
            self.currentVC?.view.center.x = -screenWidth + (self.currentVC?.view.center.x)!
            toVC.view.center = bedCenter
            }, completion: completion)
      } else {
         toVC.view.center.x = -bedCenter.x
         UIView.animate(withDuration: 0.25, animations: {
            self.currentVC?.view.center.x = screenWidth + (self.currentVC?.view.center.x)!
            toVC.view.center = bedCenter
            }, completion: completion)
      }
      
      
      
      toVC.didMove(toParentViewController: self)
      self.currentButtonTag = button.tag
   }
   
   
   
   
   func extraNavigate(fromVC: UIViewController?, toVC: UIViewController?) -> Bool {
      if fromVC == toVC {return false}
      
      if (fromVC != nil) {
         fromVC!.willMove(toParentViewController: nil)
         fromVC!.view.removeFromSuperview()
         fromVC!.removeFromParentViewController()
      }
      if (toVC != nil) {
         self.addChildViewController(toVC!)
         mainViewBed.addSubview(toVC!.view)
         toVC!.didMove(toParentViewController: self)
         currentVC = toVC
      }
      return true
   }
   
   
   
   
   func setupAddButton(imageName:String, target: AnyObject?, action: Selector) {
      addButton.removeTarget(nil, action: nil, for: .allEvents)
      addButton.setBackgroundImage(UIImage(named: imageName), for: .normal)
      addButton.addTarget(target, action:action, for: UIControlEvents.touchUpInside)
      topPanel.addSubview(addButton)
   }
   func removeAddButton() {
      addButton.removeTarget(nil, action: nil, for: .allEvents)
      addButton.setBackgroundImage(nil, for: .normal)
   }
   
   func setupTitleButton(title:String, titleColor:UIColor, font:UIFont, backgroundColor:UIColor, target: AnyObject?, action: Selector) {
      titleButton.setTitle(title, for: .normal)
      titleButton.setTitleColor(titleColor, for: .normal)
      titleButton.titleLabel?.font = font
      titleButton.backgroundColor = backgroundColor
      titleButton.addTarget(target, action:action, for: UIControlEvents.touchUpInside)
      topPanel.addSubview(titleButton)
   }
   
   
   
}

