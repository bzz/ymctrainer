import UIKit


class AddVC: UIViewController {
   var dateLabel = UILabel()
   
   
   var leftButton = UIButton()
   var rightButton = UIButton()
   let loginField = UITextField()
   let pickerView = UIPickerView()
   let repTypes = ["техника 1",
                   "техника 2",
                   "техника 3",
                   "техника 4"]
   
   
   override func viewDidLoad() {
      super.viewDidLoad()
      view.frame = (parent as! NavigationVC).mainViewBed.bounds      
      view.backgroundColor = UIColor(hexA:0xffffff20)
      

      
      let label1 = UILabel(frame:CGRect(x: 0, y: 20, width: screenWidth, height: 0.1*screenHeight))
      label1.numberOfLines = 0
      label1.text = "ДАТА"
      label1.textAlignment = .center
      label1.textColor = UIColor(hex: 0x5e99d9)
      label1.font = UIFont(name: "HelveticaNeue", size: 20)
      view.addSubview(label1)
//      
//      let label2 = UILabel(frame:CGRect(x: 0, y: 20, width: screenWidth, height: 0.1*screenHeight))
//      label2.numberOfLines = 0
//      label2.text = "ДАТА"
//      label2.textAlignment = .center
//      label2.textColor = UIColor(hex: 0x5e99d9)
//      label2.font = UIFont(name: "HelveticaNeue", size: 20)
//      view.addSubview(label2)
//
//      let label3 = UILabel(frame:CGRect(x: 0, y: 20, width: screenWidth, height: 0.1*screenHeight))
//      label3.numberOfLines = 0
//      label3.text = "ДАТА"
//      label3.textAlignment = .center
//      label3.textColor = UIColor(hex: 0x5e99d9)
//      label3.font = UIFont(name: "HelveticaNeue", size: 20)
//      view.addSubview(label3)

      
      
      
      dateLabel = UILabel(frame:CGRect(x: 0, y: 0.1*screenHeight, width: screenWidth, height: 0.1*screenHeight))
      dateLabel.numberOfLines = 0
      dateLabel.text = Date().string()
      dateLabel.textAlignment = .center
      dateLabel.textColor = UIColor(hex: 0x5e99d9)
      dateLabel.font = UIFont(name: "HelveticaNeue", size: 20)
      view.addSubview(dateLabel)

      
      leftButton   = UIButton(type: UIButtonType.system) as UIButton
      leftButton.frame = CGRect(x:0, y:0.1*screenHeight, width:0.1*screenHeight, height:0.1*screenHeight)
      leftButton.backgroundColor = UIColor(hex: 0xf7f7f7)
      leftButton.setTitle("<", for: UIControlState.normal)
      leftButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 20)
      leftButton.addTarget(self, action:#selector(leftButtonPressed), for: UIControlEvents.touchUpInside)
      view.addSubview(leftButton)

      rightButton   = UIButton(type: UIButtonType.system) as UIButton
      rightButton.frame = CGRect(x:screenWidth - 0.1*screenHeight, y:0.1*screenHeight, width:0.1*screenHeight, height:0.1*screenHeight)
      rightButton.backgroundColor = UIColor(hex: 0xf7f7f7)
      rightButton.setTitle(">", for: UIControlState.normal)
      rightButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 20)
      rightButton.addTarget(self, action:#selector(rightButtonPressed), for: UIControlEvents.touchUpInside)
      view.addSubview(rightButton)

      
      pickerView.frame = CGRect(x: 0.3*screenWidth, y: 0.2*screenHeight, width: 0.8*screenWidth, height: 0.5*screenHeight)
      pickerView.delegate = self
      pickerView.dataSource = self
      view.addSubview(pickerView)
      
      
      
      
      
      let doneButton   = UIButton(type: UIButtonType.system) as UIButton
      doneButton.frame = CGRect(x: 0.25*screenWidth, y: view.frame.height - 0.1*screenHeight, width: 0.5*screenWidth, height: 0.1*screenHeight)
      doneButton.backgroundColor = UIColor(hex: 0xf7f7f7)
      doneButton.setTitle("ДОБАВИТЬ", for: UIControlState.normal)
      doneButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 20)
      doneButton.addTarget(self, action:#selector(doneButtonPressed), for: UIControlEvents.touchUpInside)
      view.addSubview(doneButton)
      
      

   }

   func leftButtonPressed() {
      let df = DateFormatter()
      df.timeZone = TimeZone.current
      df.dateFormat = "yyyy-MM-dd"
      if let d = df.date(from: dateLabel.text!) {
         var n = d
         n.addTimeInterval(-60*60*24)
         dateLabel.text = n.string()
      }
   }
   
   func rightButtonPressed() {
      let df = DateFormatter()
      df.timeZone = TimeZone.current
      df.dateFormat = "yyyy-MM-dd"
      if let d = df.date(from: dateLabel.text!) {
         var n = d
         n.addTimeInterval(60*60*24)
         dateLabel.text = n.string()
      }
   }
   
   func doneButtonPressed() {
      
      let type = String(pickerView.selectedRow(inComponent: 1))
      var count = pickerView.selectedRow(inComponent: 0)
      
      var dic = NSMutableDictionary()
      if let d1 = history[type] as? NSMutableDictionary {
         dic = d1
         if let old = dic.value(forKey: dateLabel.text!) {
            count += (old as AnyObject).intValue
         }
      }
      dic[dateLabel.text!] = String(count)
      history[type] = dic //dic ["01.12.2001" : count]
      
      DBManager.saveDic(dic: history, userDefaultsKey: "history")

      (parent as! NavigationVC).extraNavigate(fromVC: self, toVC: StatsVC())
   }

}


extension AddVC : UIPickerViewDelegate, UIPickerViewDataSource {
   
   func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      switch component {
      case 0:
         return String(row)
      default:
         return repTypes[row]
      }
   }
   func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      switch component {
      case 0:
         return 999
      default:
         return repTypes.count
      }
   }
   
   func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
      switch component {
      case 0:
         return 0.2*screenWidth
      default:
         return 0.5*screenWidth
      }
   }
   func numberOfComponents(in pickerView: UIPickerView) -> Int {
      return 2
   }
}

