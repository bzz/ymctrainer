//
//  YMCTrainer
//
//  Created by Mikhail Baynov on 17/10/2016.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//
import UIKit


//GLOBALS
let screenHeight = UIScreen.main.bounds.height
let screenWidth = UIScreen.main.bounds.width
let appDelegate = UIApplication.shared.delegate as! AppDelegate

var history = NSMutableDictionary()  //[repType : [date : count]]


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

   var window: UIWindow?


   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      window = UIWindow(frame: UIScreen.main.bounds)
      window!.rootViewController = NavigationVC(initialButtonIndex:0)
      window!.makeKeyAndVisible()
      
      history = DBManager.loadDic(userDefaultsKey: "history")
      return true
   }



}

