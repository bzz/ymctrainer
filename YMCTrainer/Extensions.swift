import UIKit


extension UIColor {
   public convenience init(hex: UInt32, alpha: CGFloat = 1) {
      let divisor = CGFloat(255)
      let red     = CGFloat((hex & 0xFF0000) >> 16) / divisor
      let green   = CGFloat((hex & 0x00FF00) >>  8) / divisor
      let blue    = CGFloat( hex & 0x0000FF       ) / divisor
      self.init(red: red, green: green, blue: blue, alpha: alpha)
   }
   
   /**
    The six-digit hexadecimal representation of color with alpha of the form #RRGGBBAA.
    
    - parameter hex8: Eight-digit hexadecimal value.
    */
   public convenience init(hexA: UInt32) {
      let divisor = CGFloat(255)
      let red     = CGFloat((hexA & 0xFF000000) >> 24) / divisor
      let green   = CGFloat((hexA & 0x00FF0000) >> 16) / divisor
      let blue    = CGFloat((hexA & 0x0000FF00) >>  8) / divisor
      let alpha   = CGFloat( hexA & 0x000000FF       ) / divisor
      self.init(red: red, green: green, blue: blue, alpha: alpha)
   }
   
   
   /**
    Hex string of a UIColor instance.
    
    - parameter rgba: Whether the alpha should be included.
    */
   public func hexString(includeAlpha: Bool) -> String {
      var r: CGFloat = 0
      var g: CGFloat = 0
      var b: CGFloat = 0
      var a: CGFloat = 0
      self.getRed(&r, green: &g, blue: &b, alpha: &a)
      
      if (includeAlpha) {
         return String(format: "#%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
      } else {
         return String(format: "#%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
      }
   }
   

   var invertedColor: UIColor {
      var r: CGFloat = 0.0, g: CGFloat = 0.0, b: CGFloat = 0.0, a: CGFloat = 0.0
      UIColor.red.getRed(&r, green: &g, blue: &b, alpha: &a)
      return UIColor(red: 1 - r, green: 1 - g, blue: 1 - b, alpha: a) // Assuming you want the same alpha value.
   }
}


extension Date {
   func dateStringWithFormat(format: String) -> String {
      let df = DateFormatter()
      df.dateFormat = format
      return df.string(from: self)
   }
   
   func UTCString() -> String {
      let df = DateFormatter()
      df.timeZone = TimeZone(abbreviation: "UTC")
      df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
      return df.string(from: self)
   }

   func string() -> String {
      let df = DateFormatter()
      df.timeZone = .current
      df.dateFormat = "yyyy-MM-dd"
      return df.string(from: self)
   }

   /*// USAGE: - Date from UTC string
    let df = DateFormatter()
    df.timeZone = TimeZone(abbreviation: "UTC")
    df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    var dstr = ""
    if let d = df.date(from: time) {
    dstr = d.dateStringWithFormat(format: "HH:mm")
    }
    */
   
}




