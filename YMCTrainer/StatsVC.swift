import UIKit


class StatsVC: UIViewController {

   var tableView = UITableView()
   var repTypeButton = UIButton()

   
   override func viewDidLoad() {
      super.viewDidLoad()
      view.frame = (parent as! NavigationVC).mainViewBed.bounds      
      view.backgroundColor = UIColor(hexA:0xffffff50)
      

      
      tableView.frame = CGRect(x: 0, y: 20, width: view.frame.width, height: view.frame.height - 0.1*screenHeight - 20)
      tableView.delegate = self
      tableView.dataSource = self
      tableView.register(Cell.self, forCellReuseIdentifier: String(describing: Cell.self))
      tableView.backgroundColor = UIColor.clear
      tableView.showsVerticalScrollIndicator = false
      
      tableView.separatorStyle = .none
      tableView.rowHeight = UITableViewAutomaticDimension;
      //        tableView.estimatedRowHeight = 100.0;
      
      
      
      view.addSubview(tableView)
      //      setupCurtain()
      
      
      
      
      repTypeButton   = UIButton(type: UIButtonType.system) as UIButton
      repTypeButton.frame = CGRect(x: 0.25*screenWidth, y: view.frame.height - 0.1*screenHeight, width: 0.5*screenWidth, height: 0.1*screenHeight)
      repTypeButton.backgroundColor = UIColor(hex: 0xf7f7f7)
      repTypeButton.tag = 4
      repTypeButton.setTitle("Все техники", for: UIControlState.normal)
      repTypeButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 20)
      repTypeButton.addTarget(self, action:#selector(repTypeButtonPressed), for: UIControlEvents.touchUpInside)
      view.addSubview(repTypeButton)

      
      
      
   }

   func repTypeButtonPressed() {
      let actionSheet = UIActionSheet(title: "Выберите технику", delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Техника 1", "Техника 2",
                                      "Техника 3", "Техника 4", "Все техники")
      actionSheet.show(in: view)
   }
}


extension StatsVC : UIActionSheetDelegate {
   func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
      repTypeButton.tag = buttonIndex
      repTypeButton.setTitle(actionSheet.buttonTitle(at: buttonIndex), for: .normal)
      tableView.reloadData()
   }
}



extension StatsVC : UITableViewDelegate, UITableViewDataSource {

   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if repTypeButton.tag == 4 {
         let dicSum = NSMutableDictionary()
         var i = 0
         while i < 4 {
            if let dic = history.value(forKey: String(i)) {
               dicSum.addEntries(from: dic as! [AnyHashable : Any])
            }
            i += 1
        }
         return dicSum.allKeys.count

      } else {
         if let dic = history.value(forKey: String(repTypeButton.tag)) {
            return (dic as! NSDictionary).allKeys.count
         }
      }
      return 0
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: Cell.self), for: indexPath) as! Cell
      
      var dic = NSMutableDictionary()
      if repTypeButton.tag == 4 {
         var i = 0
         while i < 4 {
            if let d = history.value(forKey: String(i)) as? NSDictionary {
               for k in d.allKeys {
                  print(k)
                  print(d[k]!)
                  let str = d[k]! as! String
                  let a = Int(str)!
                  print(a)
                  
                  var b = 0
                  if let b0 = dic[k] as? String {
                     b = Int(b0)!
                  }
                  dic[k] = String(a + b)
                  
               }
            }
            i += 1
         }
         
      } else {
         dic = history.value(forKey: String(repTypeButton.tag)) as! NSMutableDictionary
      }
      cell.dateLabel.text! = dic.allKeys[indexPath.row] as! String
      cell.repsLabel.text! = dic.allValues[indexPath.row] as! String
      return cell
   }
   
   
//   func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//      return feedArray[indexPath.row].frame.size.height + 10
//   }
//   
   
//   func scrollViewDidScroll(scrollView: UIScrollView) {
//      let offsetY = scrollView.contentOffset.y;
//      let headerContentView = self.tableView.tableHeaderView!.subviews[0];
//      if offsetY < -10 {
//         headerContentView.transform = CGAffineTransform(translationX: 0, y: offsetY); //MIN(offsetY, 0)
//      } else {
//         headerContentView.transform = CGAffineTransform(translationX: 0, y: -10);
//      }
//   }
}

class Cell: UITableViewCell {
   
   var dateLabel : UILabel!
   var repsLabel : UILabel!
   
   override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.backgroundColor = UIColor.clear
      self.selectionStyle = .none

   
      dateLabel = UILabel(frame:CGRect(x:0, y:0, width: 0.5*screenWidth, height:0.1*screenHeight))
      dateLabel.text = "01.01.2001"
      dateLabel.textColor = UIColor(hex: 0x000000)
      dateLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 18)
      self.addSubview(dateLabel)

      repsLabel = UILabel(frame:CGRect(x:0.5*screenWidth, y:0, width: 0.5*screenWidth, height:0.1*screenHeight))
      repsLabel.text = "01.01.2001"
      repsLabel.textColor = UIColor(hex: 0x000000)
      repsLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 18)
      self.addSubview(repsLabel)
   }
   
   required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)!
      print("init(coder:) has not been implemented")
   }
}




